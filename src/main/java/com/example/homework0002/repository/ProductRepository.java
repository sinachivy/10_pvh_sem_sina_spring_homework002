package com.example.homework0002.repository;

import com.example.homework0002.model.entity.Customer;
import com.example.homework0002.model.entity.Product;
import com.example.homework0002.model.entity.Product;
import com.example.homework0002.model.request.CustomerRequest;
import com.example.homework0002.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("SELECT * FROM product_tb")
    @Result(property = "productId", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_Price")
    List<Product> findAllProduct();

    @Delete("""
            DELETE FROM product_tb 
            WHERE product_id=#{productId};
            """)
    boolean deleteProductById(@Param("productId") Integer productId);

    @Select("""
            SELECT * FROM product_tb WHERE product_id=#{productId}; 
            """)
    @Result(property = "productId", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_Price")
    Product getProductById(Integer productId);


    @Select("""
            insert into "product_tb"( product_name, product_price)
            values (#{request.productName},#{request.productPrice})
            """)
    Integer saveProduct (@Param("request")ProductRequest productRequest);


    @Select("""
        update product_tb
        set product_name=#{reqChange.productName},
        product_price=#{reqChange.productPrice},
        where product_id=#{productId}
        Returning product_id
        """)
    Integer update(@Param("reqChange") ProductRequest productRequest,Integer productId);

}
/*  ---Script DataBase------
*
* create table product_tb(
    product_id serial primary key ,
    product_name varchar unique ,
    product_price double precision);

create table customer_tb(
    customer_id serial primary key ,
    customer_name varchar ,
    customer_address varchar,
    customer_phone int);

create table invoice_tb(
    invoice_id serial primary key ,
    invoice_date date,
    customer_id int ,
    constraint fk_customer_fk foreign key (customer_id) references customer_tb(customer_id)
        on delete cascade on update cascade );

create table invoice_detail(
    id serial primary key ,
    product_id int,invoice_id int,
    constraint fk_product_fk foreign key (product_id) references product_tb(product_id)
        on delete cascade on update  cascade ,constraint fk_invoice_fk foreign key (invoice_id) references invoice_tb(invoice_id) on delete  cascade on update  cascade )*/

