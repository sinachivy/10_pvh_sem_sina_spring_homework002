package com.example.homework0002.repository;

import com.example.homework0002.model.entity.Customer;
import com.example.homework0002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
    @Result(property = "customerId", column = "customer_id")
    @Result(property = "customerName", column = "customer_name")
    @Result(property = "customerAddress", column = "customer_address")
    @Result(property = "customerPhone", column = "customer_phone")

    List<Customer> findAllCustomer();

    @Delete("""
            DELETE FROM customer_tb 
            WHERE customer_id=#{customerId};
            """)
    boolean deleteCustomerById(@Param("customerId") Integer customerId);

    @Select("""
            SELECT * FROM customer_tb WHERE customer_id=#{customerId}; 
            """)
    @Result(property = "customerId", column = "customer_id")
    @Result(property = "customerName", column = "customer_name")
    @Result(property = "customerAddress", column = "customer_address")
    @Result(property = "customerPhone", column = "customer_phone")
    Customer getCustomerById(Integer customerId);

    @Select("""
        update customer_tb
        set customer_name=#{reqChange.customerName},
        customer_address=#{reqChange.customerAddress},
        customer_phone=#{reqChange.customerPhone }
        where customer_id=#{customerId}
        Returning customer_id
        """)
    Integer update(@Param("reqChange") CustomerRequest customerRequest,Integer customerId);


    @Select("""
            insert into "customer_tb"( customer_name, customer_address, customer_phone)
            values (#{request.customerName},#{request.customerAddress},#{request.customerPhone})
            """)
//    @Result(property = "customerId", column = "customer_id")
//    @Result(property = "customerName", column = "customer_name")
//    @Result(property = "customerAddress", column = "customer_address")
//    @Result(property = "customerPhone", column = "customer_phone")
    Integer saveCustomer (@Param("request") CustomerRequest customerRequest);








}
