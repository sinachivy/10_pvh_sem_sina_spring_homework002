package com.example.homework0002.controller;

import com.example.homework0002.model.entity.Customer;
import com.example.homework0002.model.entity.Product;
import com.example.homework0002.model.request.CustomerRequest;
import com.example.homework0002.model.request.ProductRequest;
import com.example.homework0002.model.response.CustomerResponse;
import com.example.homework0002.model.response.ProductResponse;
import com.example.homework0002.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/product/v1")
public class ProductController {
  private final ProductRequest productRequest;
private final ProductService productService;
    public ProductController(ProductRequest productRequest, ProductService productService) {
      this.productRequest = productRequest;
        this.productService = productService;
    }
    @GetMapping("/get-all-product")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct() {
       ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .message("Fecth success")
                .payload(productService.getAllProduct())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp((System.currentTimeMillis())))
                .build();

        return ResponseEntity.ok(response);
    }


    @GetMapping("/get-product-by-id/{id}")

    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer productId) {
        ProductResponse<Product> response=null;
        if (productService.getProductByID(productId) != null) {
            response = ProductResponse.<Product>builder()
                    .message("Success ")
                    .payload(productService.getProductByID(productId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response=ProductResponse.<Product>builder()
                    .message("Data Not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }



    }
    @DeleteMapping("/delete-product-by-id/{id}")
    public ResponseEntity<ProductResponse<String>>deleteProductById(@PathVariable("id") Integer productId, @PathVariable String id) {
        ProductResponse<String> response=null;
        if(productService.deleteProductById(productId) ==true) {
            response=ProductResponse.<String>builder()
                    .message("Delete Success")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }


    @PostMapping("/add-new-product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){
        Integer storeProId=productService.addNewProduct( productRequest);
        if (storeProId !=null){
            ProductResponse<Product> response=ProductResponse.<Product>builder()
                    .message("Add success")
                    .payload(productService.getProductByID(storeProId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    @PutMapping("/update-product-by-id/{id}")
    public ResponseEntity<ProductResponse<Product>> update(
            @RequestBody ProductRequest productRequest,@PathVariable ("id") Integer productId
    ){
        ProductResponse<Product> response=null;
        System.out.println(productId);
        Integer idProductUpdate=productService.updateProduct(productRequest,productId);

        if (idProductUpdate !=null){
            response=ProductResponse.<Product>builder()
                    .message("Update Success")
                    .payload(productService.getProductByID(idProductUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}
