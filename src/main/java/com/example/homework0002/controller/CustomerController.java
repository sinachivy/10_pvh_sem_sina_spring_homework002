package com.example.homework0002.controller;

import com.example.homework0002.model.entity.Customer;
import com.example.homework0002.model.request.CustomerRequest;
import com.example.homework0002.model.response.CustomerResponse;
import com.example.homework0002.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping ("/api/v1/customers")
public class CustomerController {
private final CustomerRequest customerRequest;
    private final CustomerService customerService;

    public CustomerController(CustomerRequest customerRequest, CustomerService customerService) {
        this.customerRequest = customerRequest;
        this.customerService = customerService;
    }

    @GetMapping ("/{get-all-customer}")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer() {
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Fecth success")
                .payload(customerService.getAllCustomer())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp((System.currentTimeMillis())))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-customer-by-id/{id}")

    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable ("id") Integer customerId) {
    CustomerResponse<Customer> response=null;
        if (customerService.getCustomerByID(customerId) != null) {
             response = CustomerResponse.<Customer>builder()
                    .message("Success ")
                    .payload(customerService.getCustomerByID(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response=CustomerResponse.<Customer>builder()
                    .message("Data Not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }



    }

    @DeleteMapping ("/delete-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<String>>deleteCustomerById(@PathVariable("id") Integer customerId, @PathVariable String id) {
        CustomerResponse<String> response=null;
        if(customerService.deleteCustomerById(customerId) ==true) {
            response=CustomerResponse.<String>builder()
                    .message("Delete Success")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
    @PostMapping("/add-new-customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
    Integer storeCusId=customerService.addNewCustomer(customerRequest);
        if (storeCusId !=null){
            CustomerResponse<Customer> response=CustomerResponse.<Customer>builder()
                    .message("Add success")
                    .payload(customerService.getCustomerByID(storeCusId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> update(
            @RequestBody CustomerRequest customerRequest,@PathVariable ("id") Integer customerId
    ){
        CustomerResponse<Customer> response=null;
        System.out.println(customerId);
        Integer idCustomerUpdate=customerService.updateCustomer(customerRequest,customerId);

        if (idCustomerUpdate !=null){
            response=CustomerResponse.<Customer>builder()
                    .message("Update Success")
                    .payload(customerService.getCustomerByID(idCustomerUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}