package com.example.homework0002.service.ServiceImp;

import com.example.homework0002.model.entity.Customer;
import com.example.homework0002.model.request.CustomerRequest;
import com.example.homework0002.repository.CustomerRepository;
import com.example.homework0002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerByID(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer customerId=customerRepository.saveCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        System.out.println(customerRequest);

        Integer customerUpdate =customerRepository.update(customerRequest,customerId);
        System.out.println(customerUpdate);
        return customerUpdate;

    }


}
