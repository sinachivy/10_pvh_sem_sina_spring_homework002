package com.example.homework0002.service.ServiceImp;

import com.example.homework0002.model.entity.Product;
import com.example.homework0002.model.request.ProductRequest;
import com.example.homework0002.repository.ProductRepository;
import com.example.homework0002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImp implements ProductService{
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductByID(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId=productRepository.saveProduct(productRequest);
        return productId;
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        System.out.println(productRequest);

        Integer productUpdate =productRepository.update(productRequest,productId);
        System.out.println(productUpdate);
        return productUpdate;
    }

}
