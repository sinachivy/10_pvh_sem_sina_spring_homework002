package com.example.homework0002.service;

import com.example.homework0002.model.entity.Product;
import com.example.homework0002.model.request.CustomerRequest;
import com.example.homework0002.model.request.ProductRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public  interface ProductService {
    List<Product> getAllProduct();

    Product getProductByID(Integer productId);

    boolean deleteProductById(Integer productId);
    Integer addNewProduct(ProductRequest productRequest);
    Integer updateProduct(ProductRequest productRequest,Integer productId);
}
