package com.example.homework0002.service;

import com.example.homework0002.model.entity.Customer;
import com.example.homework0002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomer();
    Customer getCustomerByID(Integer customerId);
    boolean deleteCustomerById(Integer customerId);
    Integer addNewCustomer(CustomerRequest customerRequest);
    Integer updateCustomer(CustomerRequest customerRequest,Integer customerId);


}
