package com.example.homework0002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework0002Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework0002Application.class, args);
    }

}
