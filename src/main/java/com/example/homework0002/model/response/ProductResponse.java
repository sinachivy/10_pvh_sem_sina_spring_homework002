package com.example.homework0002.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse <T>{
    private String message;
    private T payload;
    private HttpStatus httpStatus;
    private Timestamp timestamp;
}
