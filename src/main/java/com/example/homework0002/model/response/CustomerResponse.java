package com.example.homework0002.model.response;

import com.example.homework0002.model.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerResponse<T> {
    private String message;
    private T payload;
    private HttpStatus httpStatus;
    private Timestamp timestamp;
}
